
## Generate samples

import random
import gym
import tqdm
import csv
import time
env = gym.make('CartPole-v1')


random.seed(2018)

print("Generating samples ...")

with open('cartpole.csv', 'w', newline='') as csvfile:
    samplewriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    samplewriter.writerow(["Step", "CartPos", "CartVelocity", "PoleAngle", \
                            "PoleVelocity", "Action", "Reward"])
    
    laststate = None
    for k in tqdm.trange(20):
        env.reset()
        done = False
        for i in range(100):
            env.render()
            action = env.action_space.sample()
            if i > 0:
                samplewriter.writerow((i-1,) + tuple(state) + (action,) + (reward,))
            
            time.sleep(0.08)

            # stop only after saving the state
            if done:
                time.sleep(0.5)
                break
            
            [state,reward,done,info] = env.step(action) # take a random action
            
env.close()

